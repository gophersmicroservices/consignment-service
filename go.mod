module bitbucket.org/gophersmicroservices/consignment-service

require (
	bitbucket.org/gophersmicroservices/user-service v1.0.1
	bitbucket.org/gophersmicroservices/vessel-service v1.0.3
	github.com/golang/protobuf v1.2.0
	github.com/micro/cli v0.0.0-20181223203424-1b0c9793c300
	github.com/micro/go-micro v0.14.1
	github.com/mongodb/mongo-go-driver v0.1.0
	github.com/pkg/errors v0.8.0
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
)
