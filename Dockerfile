FROM golang:1.11 as builder
WORKDIR /app/consignment-service
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/consignment-service

FROM alpine:latest

WORKDIR /app
COPY --from=builder /app/consignment-service/bin/consignment-service .
CMD ["./consignment-service"]