package main

import (
	pb "bitbucket.org/gophersmicroservices/consignment-service/proto/consignment"
	vesselProto "bitbucket.org/gophersmicroservices/vessel-service/proto/vessel"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
)

type ConsignmentStorage interface {
	Create(*pb.Consignment) error
	GetAll() ([]*pb.Consignment, error)
}

type service struct {
	repo ConsignmentStorage
	vesselClient vesselProto.VesselServiceClient
}

func (s *service) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response) error {
	vesselResponse, err := s.vesselClient.FindAvailable(context.Background(), &vesselProto.Specification{
		MaxWeight: req.Weight,
		Capacity: int32(len(req.Containers)),
	})

	if vesselResponse == nil {
		return errors.Errorf("could not find available vessel for consignment")
	}

	err = s.repo.Create(req)
	if err != nil {
		return err
	}

	res.Created = true
	res.Consignment = req
	return nil
}


func (s *service) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	consignments, err := s.repo.GetAll()
	if err != nil {
		return err
	}

	res.Consignments = consignments
	return nil
}
