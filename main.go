package main

import (
	pb "bitbucket.org/gophersmicroservices/consignment-service/proto/consignment"
	userService "bitbucket.org/gophersmicroservices/user-service/proto/user"
	vesselProto "bitbucket.org/gophersmicroservices/vessel-service/proto/vessel"
	"errors"
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/server"
	"github.com/mongodb/mongo-go-driver/mongo"
	"golang.org/x/net/context"
	"log"
	"os"
	"time"
)

type Config struct {
	MongoDSN string
}

func main() {
	var config Config

	srv := micro.NewService(
		micro.Name("go.micro.srv.consignment"),
		micro.Flags(cli.StringFlag{
			Name:        "mongo_dsn",
			Value:       "mongodb://localhost:27017",
			Usage:       "MongoDB DSN",
			EnvVar:      "MONGO_DSN",
			Destination: &config.MongoDSN,
		}),
		micro.WrapHandler(AuthWrapper))

	srv.Init()

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, config.MongoDSN)
	if err != nil {
		log.Fatalln(err)
	}

	repo := NewRepository(client)
	vesselClient := vesselProto.NewVesselServiceClient("go.micro.srv.vessel", srv.Client())
	pb.RegisterConsignmentServiceHandler(srv.Server(), &service{repo, vesselClient})

	if err := srv.Run(); err != nil {
		log.Fatalln(err)
	}
}


func AuthWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, resp interface{}) error {
		if os.Getenv("DISABLE_AUTH") == "true" {
			return fn(ctx, req, resp)
		}

		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return errors.New("no auth meta-data found in request")
		}

		token := meta["Token"]

		authClient := userService.NewUserServiceClient("go.micro.srv.user", client.DefaultClient)
		_, err := authClient.ValidateToken(context.Background(), &userService.Token{
			Token: token,
		})
		if err != nil {
			return err
		}

		err = fn(ctx, req, resp)
		return err
	}
}