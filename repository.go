package main

import (
	pb "bitbucket.org/gophersmicroservices/consignment-service/proto/consignment"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"
	"golang.org/x/net/context"
)

type ContainerDocument struct {
	ID primitive.ObjectID `bson:"_id"`
	CustomerID primitive.ObjectID `bson:"customer_id"`
	UserID primitive.ObjectID  `bson:"user_id"`
	Origin string `bson:"origin"`
}

type ConsignmentDocument struct {
	ID primitive.ObjectID `bson:"_id"`
	Weight int32 `bson:"weight"`
	Description string `bson:"description"`
	Containers []*ContainerDocument `bson:"containers"`
	VesselID primitive.ObjectID `bson:"vessel_id"`
}

func NewConsignmentDocument(c *pb.Consignment) *ConsignmentDocument {
	var containers []*ContainerDocument

	for _, container := range c.Containers {
		customerID, _ := primitive.ObjectIDFromHex(container.CustomerId)
		userID, _ := primitive.ObjectIDFromHex(container.UserId)

		containers = append(containers, &ContainerDocument{
			ID: primitive.NewObjectID(),
			CustomerID: customerID,
			UserID:     userID,
			Origin:     container.Origin,
		})
	}

	vesselID, _ := primitive.ObjectIDFromHex(c.VesselId)
	return &ConsignmentDocument{
		ID:          primitive.NewObjectID(),
		Weight:      c.Weight,
		Description: c.Description,
		Containers:  containers,
		VesselID:    vesselID,
	}
}

func (d *ConsignmentDocument) ToConsignment() *pb.Consignment {
	var containers []*pb.Container
	for _, cont := range d.Containers {
		containers = append(containers, &pb.Container{
			Id:                   cont.ID.Hex(),
			CustomerId:           cont.CustomerID.Hex(),
			Origin:               cont.Origin,
			UserId:               cont.UserID.Hex(),
		})
	}

	return &pb.Consignment{
		Id:          d.ID.Hex(),
		Description: d.Description,
		Weight:      d.Weight,
		Containers:  containers,
		VesselId:    d.VesselID.Hex(),
	}
}

type Repository struct {
	c *mongo.Collection
}

func NewRepository(client *mongo.Client) *Repository {
	return &Repository{
		c: client.Database("shippy").Collection("consignments"),
	}
}

func (repo *Repository) Create(consignment *pb.Consignment) error {
	_, err :=  repo.c.InsertOne(context.Background(), NewConsignmentDocument(consignment))
	return err
}

func (repo *Repository) GetAll() ([]*pb.Consignment, error) {
	ctx := context.Background()
	cursor, err := repo.c.Find(ctx,nil)
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)

	var consignments []*pb.Consignment
	for cursor.Next(ctx) {
		doc := &ConsignmentDocument{}
		err := cursor.Decode(&doc)
		if err != nil {
			return nil, err
		}

		consignments = append(consignments, doc.ToConsignment())
	}

	if err := cursor.Err();err != nil {
		return nil, err
	}

	return consignments, nil
}
